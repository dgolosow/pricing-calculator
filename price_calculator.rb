class PriceCalculator
  def initialize(pricing_table)
    @pricing_table = pricing_table
  end

  # Calculates details and totals of items bought
  # @param [Array<String>] items an unordered list of items
  def detailed_calculation(items)
    item_types = items.uniq
    items_bought = @pricing_table.select { |item| item_types.include?(item[:item]) }
    unavailable_items = item_types.reject { |item| available_items.include?(item) }
    item_totals = items_bought.map do |item|
      count = item_count(items, item[:item])
      price = calculate_price(item, count)
      full_price = calculate_price(item, count, false)
      { item: item[:item], total: price, full_price: full_price, savings: full_price - price, quantity: count }
    end

    {
      total: item_totals.map {|i| i[:total] }.reduce(:+).to_f,
      savings: item_totals.map {|i| i[:savings] }.reduce(:+).to_f,
      full_price: item_totals.map {|i| i[:full_price] }.reduce(:+).to_f,
      items: item_totals,
      unavailable: unavailable_items
    }
  end

  def available_items
    @available_items ||= @pricing_table.map { |item| item[:item] }
  end

  # Calculates the total price of the list of items
  # @param [Array<String>] items an unordered list of items
  # @param [Boolean] discounted whether to calculate with sale prices
  def calculate_total(items, discounted = true)
    @pricing_table.reduce(0.0) do |total, item|
      count = item_count(items, item[:item])
      price = calculate_price(item, count, discounted)
      total + price
    end
  end

  # Calculates the total amount saved over
  # @param [Array<String>] items an unordered list of items
  def total_savings(items)
    full_price = calculate_total(items, false)
    discounted_price = calculate_total(items, true)
    full_price - discounted_price
  end

  # Calculate the total price for a give number of a certain item
  # @param [Hash] item the pricing definition for the item
  # @param [Integer] count the number of items
  # @param [Boolean] discounted whether to calculate with sale prices
  def calculate_price(item, count, discounted = true)
    return count * item[:unit_price] unless discounted

    discount_unit = item[:discount_unit] || 1
    price = item[:discount_price] || item[:unit_price]
    discounted_price = (count / discount_unit) * price
    regular_price = (count % discount_unit) * item[:unit_price]
    discounted_price + regular_price
  end

  # Calculates the total number of items of the given type
  # @param [Array<String>] items an unordered list of items
  # @param [String] item_type identifier of item to count
  def item_count(items, item_type)
    items.select { |item| item == item_type }.count
  end
end

