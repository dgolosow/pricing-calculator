class ReceiptPrinter
  def print_receipt(details)
    formatted = format_details(details)
    unless details[:items].empty?
      puts "#{'ITEM'.ljust(10)}#{'QTY'.ljust(10)}#{'PRICE'.ljust(10)}#{'REG PRICE'.ljust(10)}#{'SAVINGS'.ljust(10)}"
      puts '--------------------------------------------------------------------'
      formatted[:items].each do |item|
        puts "#{item[:item]}#{item[:quantity]}#{item[:total]}#{item[:full_price]}#{item[:savings]}"
      end
      puts '--------------------------------------------------------------------'
      puts "#{'TOTAL:'.ljust(20)}#{formatted[:total]}#{formatted[:full_price]}#{formatted[:savings]}"
      puts
    end

    unless details[:unavailable].empty?
      puts 'Some of the requested items are currently unavailable.'
      puts "Unavailable: #{details[:unavailable].join(', ')}"
      puts
    end
  end

  def format_details(details)
    formatted_items = details[:items].map do |item|
      {
        item: item[:item].ljust(10),
        total: format_currency_item(item[:total]),
        full_price: format_currency_item(item[:full_price]),
        savings: format_currency_item(item[:savings]),
        quantity: format_number_item(item[:quantity])
      }
    end

    {
      total: format_currency_item(details[:total]),
      savings: format_currency_item(details[:savings]),
      full_price: format_currency_item(details[:full_price]),
      items: formatted_items
    }
  end

  def format_number_item(value, rounding: 2, padding: 10, prefix: nil, suffix: nil, decimal: nil)
    formatted_value = value.round(rounding).to_s
    if decimal
      parts = formatted_value.split('.')
      formatted_value = "#{parts[0]}.#{parts[1].to_s.ljust(decimal, '0')}"
    end
    formatted = "#{prefix}#{formatted_value}#{suffix}"
    formatted.ljust(padding)
  end

  def format_currency_item(value, currency: '$', rounding: 2, padding: 10)
    format_number_item(value, prefix: currency, rounding: rounding, padding: padding, decimal: 2)
  end
end
