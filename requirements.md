# Requirements

## Business Requirements
- Items are sold by quantity
- Items may be priced at a discount for multiple items
- Items can be accepted in any order
- Discounts apply only to the specified multiple
- Items not included in the discount grouping are priced as single units
  
## Functional Requirements
- Program should receive input as an unordered list of items
- Program should calculate the total price for the given items
- Program should print the total price
- Program need only account for the items in the pricing table

### Pricing Table
```
Item     Unit price        Sale price
--------------------------------------
Milk      $3.97            2 for $5.00
Bread     $2.17            3 for $6.00
Banana    $0.99
Apple     $0.89
```

### Example Output
```
$ ruby price_calculator.rb
Please enter all the items purchased separated by a comma
milk,milk, bread,banana,bread,bread,bread,milk,apple

Item     Quantity      Price
--------------------------------------
Milk      3            $8.97
Bread     4            $8.17
Apple     1            $0.89
Banana    1            $0.99  

Total price : $19.02
You saved $3.45 today.

```