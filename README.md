# Grocery Price Calculator

## Overview
Grocery Price Calculator calculates the total price and breakdown for a given shopping list.

## Usage

### Running
The program can be run by invoking the `main.rb` script
``` bash
$ ruby main.rb
```

### Input
The user should provide a comma separated list of each individual item purchased.  For example, if the user bought 1 apple and 3 loaves for bread the input would be as follows
``` bash
What items did you buy?
Apple,Bread,Bread,Bread
```

Items that are unavailable will be ignored.

If no items are specified the program will exit.