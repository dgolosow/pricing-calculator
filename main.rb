require_relative 'price_calculator'
require_relative 'receipt_printer'

def get_user_input
  gets.split(',')
       .map(&:strip)
       .reject(&:empty?)
       .map(&:upcase)
end

def user_shopping_list
  puts 'What items did you buy?'
  get_user_input
end

def go_shopping(calculator:, printer:)
  items = user_shopping_list
  until items.empty?
    details = calculator.detailed_calculation(items)
    printer.print_receipt(details)
    items = user_shopping_list
    puts
  end
end

pricing_table = [
  { item: 'MILK', unit_price: 3.97, discount_price: 5.00, discount_unit: 2 },
  { item: 'BREAD', unit_price: 2.17, discount_price: 6.00, discount_unit: 3 },
  { item: 'BANANA', unit_price: 0.99 },
  { item: 'APPLE', unit_price: 0.89 }
]

go_shopping(
  calculator: PriceCalculator.new(pricing_table),
  printer: ReceiptPrinter.new
)

puts 'Thanks for shopping with us!'
